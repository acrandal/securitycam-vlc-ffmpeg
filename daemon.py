#!/usr/bin/env python3

# NOT NEEDED for the system anymore
# Kept to remind me how to kill child processes

import subprocess
import signal, os
import time

g_done = False

def handler(signum, frame):
    global g_done
    print('Signal handler called with signal - setting done flag', signum)
    g_done = True

signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGHUP, handler)
signal.signal(signal.SIGINT, handler)

#** Record a single video of x seconds in length
def do_a_recording(recording_length_seconds_long):
    global g_done
    start_time = time.time()    # Grab recording start time
    
    # Start recording - Popen is non-blocking by default
    proc1 = subprocess.Popen("./record_v01.sh", shell=False, stdout=None)

    while(not g_done and (start_time + recording_length_seconds_long > time.time())):
        time.sleep(1)

    subprocess.run(["pkill", "-P", str(proc1.pid)])
    print("Killed recording process.")


if __name__ == "__main__":
    print("Starting up operations")

    secs_in_hour = 60 * 60

    while(not g_done):
        do_a_recording(2 * secs_in_hour)           # 2 hours

    time.sleep(2)
    print("Done with operations")
