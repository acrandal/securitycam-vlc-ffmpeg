#!/bin/bash

vlc --intf dummy http://admin:@10.0.0.12/video.cgi --sout="#std{access=file,mux=ogg,dst=-}" | ffmpeg -i -  -vcodec libx264 -crf 20 -vf drawtext="expansion=strftime:fontfile='/usr/share/fonts/truetype/ubuntu/UbuntuMono-R.ttf':fontsize=14:fontcolor=white:shadowcolor=black:shadowx=2:shadowy=1:text='%Y-%m-%d %H\:%M\:%S':x=8:y=8" -segment_time 01:00:00 -f segment -y -reset_timestamps 1 -segment_format mp4 -strftime 1 Recording-%Y-%m-%d_%H-%M-%S.mp4

