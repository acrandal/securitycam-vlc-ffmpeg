# SecurityCam-VLC-FFMPEG

A quick script & hack to get vlc to snag a stream from my D-Link DLC-930L webcam on the local network, pass that stream to ffmpeg for real time compression. The ffmpeg stream also includes embedding timestamps.